<!DOCTYPE html>
<html>
<head>
<style>
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}
</style>
</head>
<body>

<h2>Add a border to a table:</h2>

<table>
  <tr>
    <th>From:</th>
    <th>Date:</th>
	<th>Subject:</th>
  </tr>
  <tr>
    <td>John Diggle</td>
    <td>12/2/2016</td>
	<td>Resignation</td>
  </tr>
  <tr>
    <td>Sarah</td>
    <td>Lance</td>
	<td>Time Travel</td>
  </tr>
</table>

</body>
</html>
